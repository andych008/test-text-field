package wang.unclecat.textfieldhelper;

import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.utils.Rect;

/**
 * 适配器，适配不同的rootLayout处理移动焦点控件的差异，通过适配器来统一TextFieldHelper中的操作
 */
public interface ITFHAdapter {
    //焦点TextField与软键盘上边的距离
    default int offset() {
        return 50;
    }

    //找到rootLayout。rootLayout是ScrollView或其它，moveY的计算也是不一样的
    ComponentContainer findRootLayout(ComponentContainer uiContent);

    //计算 move 距离 moveY
    int calculateMoveY(Rect decorRect, Component focusView);

    //移动焦点TextField到软键盘上方
    void move(int moveY, boolean isKBShow);
}
