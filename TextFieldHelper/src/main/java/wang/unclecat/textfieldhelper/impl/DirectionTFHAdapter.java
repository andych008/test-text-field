package wang.unclecat.textfieldhelper.impl;

import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.utils.Rect;
import wang.unclecat.textfieldhelper.ITFHAdapter;

/**
 * 根布局为DirectionalLayout
 */
public class DirectionTFHAdapter implements ITFHAdapter {

    private ComponentContainer rootLayout;
    private Component addComponent;

    @Override
    public ComponentContainer findRootLayout(ComponentContainer uiContent) {
        this.rootLayout = uiContent;
        if (rootLayout != null) {
            if (addComponent == null) {
                addComponent = new Component(rootLayout.getContext());
                rootLayout.addComponent(addComponent);
                addComponent.setHeight(1);
            }
        }
        return this.rootLayout;
    }

    @Override
    public int calculateMoveY(Rect decorRect, Component focusView) {
        if (focusView != null) {
            int focusTop = focusView.getLocationOnScreen()[1];//焦点控件的左上角
            //int moveY = focusTop + focusView.getHeight() - decorRect.top - decorRect.getHeight() + 100;//
            int moveY = focusTop + focusView.getHeight() - decorRect.top - decorRect.getHeight();
//            LogUtil.debug(TAG, String.format("processEvent() called focusTop = %d, moveY = %d", focusTop, moveY));

            return moveY;
        }
        return 0;
    }


    @Override
    public void move(int moveY, boolean isKBShow) {
        if (isKBShow) {
            if (addComponent != null) {
                addComponent.setHeight(addComponent.getHeight()+ moveY +offset());
            }
        } else {
            if (addComponent != null) {
                addComponent.setHeight(1);
            }
        }
    }
}
