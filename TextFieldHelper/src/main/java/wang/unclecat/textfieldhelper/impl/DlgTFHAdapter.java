package wang.unclecat.textfieldhelper.impl;

import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.ComponentParent;
import ohos.agp.components.ScrollView;
import ohos.agp.utils.Rect;

/**
 * CommonDialog根布局为ScrollView
 */
public class DlgTFHAdapter extends ScrollViewTFHAdapter {

    @Override
    protected ScrollView findScrollView(ComponentContainer uiContent) {
        ComponentParent parent = uiContent.getComponentParent();
        while (parent != null) {
            if (parent instanceof ScrollView) {
                return (ScrollView) parent;
            }
            parent = parent.getComponentParent();
        }
        return null;
    }

    @Override
    public int calculateMoveY(Rect decorRect, Component focusView) {
        int i = super.calculateMoveY(decorRect, focusView);
        return i + (decorRect.getHeight()) / 2;//todo :不太准确
    }
}
