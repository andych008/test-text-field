package wang.unclecat.textfieldhelper.impl;

import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.ScrollView;
import ohos.agp.utils.Rect;
import wang.unclecat.textfieldhelper.ITFHAdapter;

/**
 * 根布局为ScrollView：ScrollView的子元素必须是DirectionalLayout
 */
public class ScrollViewTFHAdapter implements ITFHAdapter {

    protected ComponentContainer rootLayout;
    protected Component addComponent;

    @Override
    public ComponentContainer findRootLayout(ComponentContainer uiContent) {
        this.rootLayout = findScrollView(uiContent);
        if (rootLayout != null) {
            //足够长，才能滚起来
            if (addComponent == null) {
                addComponent = new Component(rootLayout.getContext());
                //必须是线性布局
                DirectionalLayout componentAt = (DirectionalLayout) rootLayout.getComponentAt(0);
                componentAt.addComponent(addComponent);
                addComponent.setHeight(1);
            }
        }
        return this.rootLayout;
    }

    protected ScrollView findScrollView(ComponentContainer uiContent) {
        if (uiContent instanceof ScrollView) {
            return (ScrollView) uiContent;
        } else {
            int count = uiContent.getChildCount();
            for (int i = 0; i < count; i++) {
                Component component = uiContent.getComponentAt(i);
                if (component instanceof ScrollView) {
                    return (ScrollView) component;
                }
            }
        }
        return null;
    }

    @Override
    public int calculateMoveY(Rect decorRect, Component focusView) {
        if (focusView != null) {
            int focusTop = focusView.getLocationOnScreen()[1];//焦点控件的左上角
            //int moveY = focusTop + focusView.getHeight() - decorRect.top - decorRect.getHeight() + 100;//
            int moveY = focusTop + focusView.getHeight() - decorRect.top - decorRect.getHeight();
//            LogUtil.debug(TAG, String.format("processEvent() called focusTop = %d, moveY = %d", focusTop, moveY));

            return moveY;
        }
        return 0;
    }


    @Override
    public void move(int moveY, boolean isKBShow) {
        if (isKBShow) {
            if (addComponent != null) {
                addComponent.setHeight(addComponent.getHeight()+ moveY +offset());
//                addComponent.setHeight(600);
            }
        } else {
            if (addComponent != null) {
                addComponent.setHeight(1);
            }
        }
        //dialog中必须延时
        rootLayout.getContext().getUITaskDispatcher().delayDispatch(new Runnable() {
            @Override
            public void run() {
                ((ScrollView) rootLayout).fluentScrollByY(moveY + offset());
            }
        }, 50);
    }
}
