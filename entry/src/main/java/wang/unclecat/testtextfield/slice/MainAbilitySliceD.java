package wang.unclecat.testtextfield.slice;

import ohos.agp.components.ComponentContainer;
import ohos.agp.components.ScrollView;
import ohos.agp.components.Text;
import timber.log.Timber;
import wang.unclecat.testtextfield.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import wang.unclecat.textfieldhelper.TextFieldHelper;
import wang.unclecat.textfieldhelper.impl.DirectionTFHAdapter;
import wang.unclecat.textfieldhelper.impl.ScrollViewTFHAdapter;

public class MainAbilitySliceD extends AbilitySlice {

    private TextFieldHelper textFieldHelper = new TextFieldHelper(new DirectionTFHAdapter());

    private int count = 0;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main2);

        Text helloText = (Text) findComponentById(ResourceTable.Id_text_helloworld);

        ComponentContainer uiContent = (ComponentContainer) findComponentById(ResourceTable.Id_root);
        textFieldHelper.monitorLayoutRefreshed(getWindow(), uiContent);

        textFieldHelper.setSoftInputMonitor(new TextFieldHelper.SoftInputMonitor() {
            @Override
            public void onSoftInputVisible(boolean isVisible) {
                Timber.d("onSoftInputVisible() called with: isVisible = [ %s ]", isVisible);
                helloText.setText(String.format("%d:%b",++count,isVisible));
            }
        });
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
