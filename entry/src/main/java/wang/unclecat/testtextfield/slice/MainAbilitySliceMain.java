package wang.unclecat.testtextfield.slice;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.IntentParams;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Component;
import ohos.utils.net.Uri;
import wang.unclecat.testtextfield.ResourceTable;
import wang.unclecat.testtextfield.dialog.CustomDialog;

import java.util.HashSet;
import java.util.Set;

public class MainAbilitySliceMain extends AbilitySlice {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        findComponentById(ResourceTable.Id_go2ScrollView).setClickedListener(component -> present(new MainAbilitySliceScroll(), new Intent()));
        findComponentById(ResourceTable.Id_go2D).setClickedListener(component -> present(new MainAbilitySliceD(), new Intent()));

        findComponentById(ResourceTable.Id_go2Dlg).setClickedListener(component -> {
            CustomDialog customDialog = new CustomDialog(this);
            customDialog.setTitle("This Is Custom Dialog");
            customDialog.setAutoClosable(true);
            customDialog.show();
        });
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
