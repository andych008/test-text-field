package wang.unclecat.testtextfield;

import ohos.aafwk.ability.AbilityPackage;
import timber.log.Timber;

public class MyApplication extends AbilityPackage {
    @Override
    public void onInitialize() {
        super.onInitialize();
        //System.out|System.err|FATAL|AndroidRuntime|01F00/
        Timber.plant(new Timber.DebugTree(0x001f00));
        Timber.d("onInitialize() called");
    }
}
