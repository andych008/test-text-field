package wang.unclecat.testtextfield;

import wang.unclecat.testtextfield.slice.MainAbilitySliceMain;
import wang.unclecat.testtextfield.slice.MainAbilitySliceScroll;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class MainAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(MainAbilitySliceMain.class.getName());
    }
}
