/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package wang.unclecat.testtextfield.dialog;

import ohos.agp.components.*;
import ohos.agp.utils.TextAlignment;
import ohos.agp.window.dialog.CommonDialog;
import ohos.agp.window.service.WindowManager;
import ohos.app.Context;
import timber.log.Timber;
import wang.unclecat.textfieldhelper.impl.DlgTFHAdapter;
import wang.unclecat.testtextfield.ResourceTable;
import wang.unclecat.textfieldhelper.TextFieldHelper;

import static ohos.agp.components.ComponentContainer.LayoutConfig.MATCH_CONTENT;

/**
 * CustomDialog
 */
public class CustomDialog extends CommonDialog {

    private Component customComponent;
    private TextField checkCode1;
    private TextField checkCode2;

    private Text titleText;
    private Component confirmButton;
    private Context context;
    private TextFieldHelper textFieldHelper;

    /**
     * CustomDialog
     *  @param abilityContext Context
     *
     */
    public CustomDialog(Context abilityContext) {
        super(abilityContext);
        this.context = abilityContext;
        this.textFieldHelper = new TextFieldHelper(new DlgTFHAdapter());
        setAlignment(TextAlignment.CENTER);
        setSize(984, MATCH_CONTENT);
        initComponents();
    }

    private void initComponents() {
        customComponent = LayoutScatter.getInstance(context)
                .parse(ResourceTable.Layout_custom_dialog_content, null, false);

//        customComponent = LayoutScatter.getInstance(context)
//                .parse(ResourceTable.Layout_ability_main, null, true);
        super.setContentCustomComponent(customComponent);
//        checkCode1 = (TextField) customComponent.findComponentById(ResourceTable.Id_num_1_textfield);
//        checkCode2 = (TextField) customComponent.findComponentById(ResourceTable.Id_num_2_textfield);
//
//        titleText = (Text) customComponent.findComponentById(ResourceTable.Id_title_text);
//        confirmButton = customComponent.findComponentById(ResourceTable.Id_confirm_button);
//
//
//        confirm();


    }

    @Override
    protected void onShow() {
        super.onShow();

        Timber.d("onShow() called");
  }

    @Override
    protected void onWindowConfigUpdated(WindowManager.LayoutConfig configParam) {
        super.onWindowConfigUpdated(configParam);
        Timber.d("onWindowConfigUpdated() called with: configParam = [ %s ]", configParam.y);

    }

    @Override
    protected void onWindowSelectionUpdated(boolean isSelectionUpdated) {
        super.onWindowSelectionUpdated(isSelectionUpdated);
        Timber.d("onWindowSelectionUpdated() called with: isSelectionUpdated = [ %s ]", isSelectionUpdated);
    }

    @Override
    protected void onShowing() {
        super.onShowing();
        Timber.d("onShowing() called");
        textFieldHelper.monitorLayoutRefreshed(getWindow(), (ComponentContainer) getContentCustomComponent(), 150);
    }

    /**
     * set title
     *
     * @param string String
     */
    public void setTitle(String string) {
//        titleText.setText(string);
    }


    private void confirm() {
        confirmButton.setClickedListener(component -> {
          destroy();
        });
    }

}
